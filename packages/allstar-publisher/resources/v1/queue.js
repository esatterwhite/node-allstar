/*jshint laxcomma: true, smarttabs: true, node:true, mocha: true*/
'use strict';
/**
 * API for the submission queue
 * @module allstar-publisher/resource/v1/queue
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires tastypie
 * @requires moduleB
 * @requires moduleC
 */

var tastypie = require('tastypie')
  , Resource = require( 'tastypie-rethink' )
  , QueueItem = require('../../models/queueitem')
  , Queue
  , async = require('async')
  ;



/**
 * Description
 * @constructor
 * @alias module:queue.js
 * @param {TYPE} [param]
 * @param {TYPE} [?param.val=1j]
 * @example var x = new require('queue.js');
 */

Queue = Resource.extend({

    options : {
        name      : 'queue'
        ,pk       : 'publisher_queueditem_id'
        ,queryset :  QueueItem.filter({}).getJoin()
        ,allowed : {
            list   : { get:true }
          , detail : { get: true }
        }
    }

    ,fields:{
        created: { type:'date', attribute:'created_at' }
      , editor:{ type:'field'}
    }

    ,constructor: function( options ){
        this.parent('constructor', options )
    }

});

module.exports = Queue;

