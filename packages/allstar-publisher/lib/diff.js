var diff_match_patch = require('diff-match-patch')
  , DMP = new diff_match_patch()
  ;

exports.as_text = function makeDiffText( new_text, old_text ){
	return DMP.patch_toText( exports.as_patch( new_text, old_text ) );
};

exports.as_patch = function( new_text, old_text ){
	return DMP.patch_make( new_text, old_text );
}
