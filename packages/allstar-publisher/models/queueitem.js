/*jshint laxcomma: true, smarttabs: true, node:true, mocha: true, esnext:true*/
'use strict';
/**
 * Represents a single change to a document with content diff and roll back functionality
 * The queueditem is the crux of the wiki system.
 * This model stores the data about items that have been edited
 * or in the event someone wishes to add new content, the QueuedItem
 * will hold the data about the propsed object and the object it is to
 * be associated with until the new object is/is not created.
 *
 * when the body of a document is edited, we do not want to to go
 * live on the site. The QueuedItem model is a generic item that
 * holds a reference to: models.Model
 *
 * the item that was edited
 * the current revision of the reverence item
 * ( incase an edit is accepted after this was submitted )
 *
 * The body as is ( HTML CODE INCLUDED )
 *
 * This is really only for editing the main body of a document.
 * @module allstar-publisher/models/queueditem
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires co
 * @requires diff-match-patch
 * @requires skoodge
 * @requires zip/lib/db
 */

var database         = require( 'zim/lib/db' ).connection
  , co               = require('co')
  , slugify          = require('gaz/string').slugify
  , stripHtmlTags    = require('gaz/string').stripHtmlTags
  , User             = require( 'skoodge' ).getUserModel()
  , ChangeSet        = require('./changeset')
  , DiffMatchPatch   = require('diff-match-patch')
  , diff             = require("../lib/diff")
  , r                = database.default.r
  , fields           = database.default.type
  , QueuedItem
  ;

QueuedItem = database.default.createModel('publisher_queueditem',{
	  created_at         : fields.date().default( ()=>{return new Date();} )
   , editor_id         : fields.string()
   , moderator_id      : fields.string()

     // Generic relation
   , content_type      : fields.string().allowNull( true ).default( null )
   , object_id         : fields.string().allowNull( true ).default( null )

   , submit_date       : fields.date().default( ()=>{return new Date(); } )
   , resubmit_date     : fields.date().default( null )
   , moderation_date   : fields.date().default( null )
   , content           : fields.string().default( null )
   , at_revision       : fields.number().min(0).default( 0 )
   , add_key           : fields.string().max( 300 ).default( null )
   , comment           : fields.string().max( 300 )

   , add_title         : fields.string().max( 200 ).default( null )
   , add_summary       : fields.string().default( null )

   , action            : fields.string().default( null ).enum([ 'edit', 'create' ])

   , moderate          : fields.string().enum(['approve', 'deny', 'open']).default( null )
   , moderation_reason : fields.string().max( 300 ).default( null )


   , moderated : fields.virtual().default(()=>{
      return this.moderate === 'open';
   })

   , is_new : fields.virtual().default(()=>{
      return this.action === 'create';
   })

   , title: fields.virtual().default(co.wrap(function*(){
      if( !this.object_id ){
        return this.add_title;
      }

      let object = yield this.get_object();
      return object.title;

   }))

   , contenttype: fields.virtual().default(()=>{
      return this.content_type
   })

   , outcome: fields.virtual().default(()=>{
      switch( this.moderate ){
        case 'approve' : return 'positive';
        case 'denial'  : return 'negetive';
        default        : return null;
      }
   })

   , document_type: fields.virtual().default(()=>{
      return this.object_id ? this.content_type : this.add_type;
   })

   , resubmitted: fields.virtual().default(()=>{
      return this.resubmit_date != null;
   })

},{
  pk:'publisher_queueditem_id'
});

/**
 * DESCRIPTION
 * @method queueitem.js#<METHODNAME>
 * @param {TYPE} NAME DESCRIPTION
 * @param {TYPE} NAME DESCRIPTION
 * @return {TYPE} DESCRIPTION
 **/
QueuedItem.define('get_object', function(){
  return r.table( this.content_type ).get( this.object_id ).run();
});

/**
 * DESCRIPTION
 * @method queueitem.js#<METHODNAME>
 * @param {TYPE} NAME DESCRIPTION
 * @param {TYPE} NAME DESCRIPTION
 * @return {TYPE} DESCRIPTION
 **/
QueuedItem.define('get_absolute_url', co.wrap(function*(){
  let object = yield this.get_object();
  return object.get_absolute_url();
}));

QueuedItem.define('is_new', function(){
  return this.action === 'create';
});

/**
 * DESCRIPTION
 * @method queueitem.js#<METHODNAME>
 * @param {TYPE} NAME DESCRIPTION
 * @param {TYPE} NAME DESCRIPTION
 * @return {TYPE} DESCRIPTION
 **/
QueuedItem.define('review_content', co.wrap(function*(){
  let object = yield this.get_object();
  return this.at_revision === 1 ? object.content : object.preview_at( this.at_revision );
}));

QueuedItem.define('diff_html', co.wrap(function*(){
  var dmp
    , object
    , changes
    , html
    , patch
    ;

  dmp = new DiffMatchPatch();
  object = yield this.get_object();

  changes = yield ChangeSet.filter({
    content_type: 'publisher_queueditem'
    ,object_id: this.publisher_queueditem_id
  }).orderBy( r.desc( 'revision' ) );

  for(let change of changes ){
    html = html || object.content;
    patch = dmp.patch_fromText( change.content_dif );
    html = dmp.patch_apply( patch, html )[0];
  }
  return html;
}));

QueuedItem.defineStatic('latest',co.wrap(function*(){
  try{
    return this.orderBy( r.desc( 'created_at') ).limit(1).nth(0)
  } catch( err ){
    return null;
  }
}));


QueuedItem.ensureIndex( 'created_at' );
QueuedItem.ensureIndex( 'content_type' );
QueuedItem.ensureIndex( 'object_id' );

QueuedItem.hasOne(User, 'editor', 'editor_id', 'auth_user_id');
QueuedItem.hasOne(User, 'modirator', 'moderator_id', 'auth_user_id');

module.exports = QueuedItem;
