/*jshint laxcomma: true, smarttabs: true, node:true, mocha: true,esnext: true*/
'use strict';
/**
 * Represents a single change to a document with content diff and roll back functionality
 * @module allstar-publisher/models/changeset
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires co
 * @requires diff-match-patch
 * @requires skoodge
 * @requires zip/lib/db
 */

var database = require( 'zim/lib/db' ).connection
  , co       = require('co')
  , r        = database.default.r
  , fields   = database.default.type
  , User     = require( 'skoodge' ).getUserModel()
  , DMP      = require( 'diff-match-patch' )
  , ChangeSet
  ;

const diff   = new DMP();

/**
 * Description
 * @constructor
 * @alias module:changeset.js
 * @param {TYPE} [param]
 * @param {TYPE} [?param.val=1j]
 * @example var x = new require('changeset.js');
 */
ChangeSet = database.default.createModel('publisher_changeset', {
	created_at   : fields.date().default( ()=>{ return new Date(); })
  , user_id      : fields.string()
  , revision     : fields.number().default(0).required(true)
  , comment      : fields.string()
  , content_diff : fields.string()
  , old_title    : fields.string()

    // Generic relation
  , content_type: fields.string().required(true)
  , object_id: fields.string().required(true)
},{
	pk:'publisher_changeset_id'
});

ChangeSet.define('reapply', co.wrap(function*( editor ){
	var content, old_content, old_title, patch;

	let editable_object = yield this.get_object();
	let changes = yield this.model.where({
						content_type:this.content_type,
						object_id: this.object_id
					})
					.filter(function( change ){
						return change('revision').gt( this.revision );
					})
					.orderBy(r.desc('revision'));

	for( let change of changes ){
		if( !content ){
			content = editable_object.html();
		}

		patch = diff.patch_fromText( change.content_diff );
		content = diff.patch_apply( patch, content )[0];
		change.reverted = true;
		yield change.save();
	}

	old_content = editable_object.html();
	old_title = editable_object.title;

	editable_object.body = content;
	yield editable_object.save();

	return editable_object.make_revision({
		content:old_content
		,title:old_title
		,comment:`Reverted to revision ${this.revision}`
		,editor: editor
	});
}));


/**
 * Returns the html string of a change
 * @method changeset.js#<METHODNAME>
 * @param {TYPE} NAME DESCRIPTION
 * @param {TYPE} NAME DESCRIPTION
 * @return {TYPE} DESCRIPTION
 **/
ChangeSet.define('display_change_html', co.wrap(function*(){
	let doc, old_content, recent_changes, next_revision_content,diffs;

	doc = yield this.get_obect();
	old_content = doc.get_html_content();

	recent_changes = yield ChangeSet.where({
		content_type: this.content_typ,
		object_id: this.object_id
	})
	.filter(( change )=>{
		change('revision').gte( this.revision);
	})
	.orderBy(r.desc('revision'));

	for( let idx=0, len=recent_changes.length; idx < len; idx++){
		// patch from current change
		let patches = diff.patch_fromText( recent_changes[idx].content_diff );
		if( len == idx+1 ){
			next_revision_content = this.old_content;
		}

		old_content = diff.patch_apply( patches, old_content )[0];
	}

	diffs = diff.diff_main( old_content, next_revision_content,{checklines:false});

	return diff.diff_prettyHtml( diffs );
}));

/**
 * applys all patches from a given point, applies them and returns the result
 * @method changeset.js#<METHODNAME>
 * @param {TYPE} NAME DESCRIPTION
 * @param {TYPE} NAME DESCRIPTION
 * @return {TYPE} DESCRIPTION
 **/
ChangeSet.define('see_item_at_version',co.wrap(function*(){
	let doc, changes, content, patch;

	doc = yield this.get_obect();
	changes = yield ChangeSet.filter(( change )=>{
		return change('content_object')
			.eq( doc.getModel().getTableName() )
			.and( change('object_id').eq( doc.id ) )
			.and( change('revision').gte( this.revision ) )
			.orderBy( r.desc( 'revision' ) );
	});

	for(let change of changes){
		content = content || doc.get_html_content();
		patch   = diff.patch_fromText( change.content_dif );
		content = diff.patch_appy( patch, content )[0];
	}

	return content;

}));

/**
 * DESCRIPTION
 * @method changeset.js#<METHODNAME>
 * @param {TYPE} NAME DESCRIPTION
 * @param {TYPE} NAME DESCRIPTION
 * @return {TYPE} DESCRIPTION
 **/
ChangeSet.define('associate', function( object ){
	this.content_type = object.model.getTableName();
	this.objectx_id = object.id;
});

/**
 * DESCRIPTION
 * @method changeset.js#<METHODNAME>
 * @param {TYPE} NAME DESCRIPTION
 * @param {TYPE} NAME DESCRIPTION
 * @return {TYPE} DESCRIPTION
 **/
ChangeSet.define('get_object',co.wrap(function(){
	return r.table( this.content_type ).get( this.object_id ).run();
}));


/**
 * DESCRIPTION
 * @method changeset.js#<METHODNAME>
 * @param {TYPE} NAME DESCRIPTION
 * @param {TYPE} NAME DESCRIPTION
 * @return {TYPE} DESCRIPTION
 **/
ChangeSet.define('display_diff_html', co.wrap(function*(){
	let that, doc, changes, html, patch;

	that    = this;
	doc     = yield this.get_object();
	changes = yield ChangeSet.filter((doc)=>{
		return doc('revision').gt( this.revision );
	}).orderBy( r.desc( 'revision' ) );

	if( changes.length ){
		for( let change of changes ){
			html = html || doc.get_html_content();
			patch = diff.patch_fromText( changes.content_diff );

			html = diff.patch_apply( patch, html )[0];
		}
	} else {
		html = doc.get_html_content();
	}

	return diff.diff_prettyHtml(
		diff.diff_main(
			html
		  , doc.get_html_content,{checklines:false}
		)
	);

}));

ChangeSet.ensureIndex('content_type');
ChangeSet.ensureIndex('object_id');
ChangeSet.ensureIndex('revision');

ChangeSet.pre('save',co.wrap(function*( next ){
	let revisions = yield ChangeSet.filter(( change )=>{
		return change( 'content_type' )
				.eq( this.content_type )
				.and( change('object_id').eq( this.object_id ));
	})
	.orderBy(r.desc('revision'));

	this.revision = revisions.length ? revisions[0].revision + 1 : 1;
	return next();
}));
ChangeSet.hasOne( User, 'editor', 'id', 'user_id' );
module.exports = ChangeSet;
