/*jshint laxcomma: true, smarttabs: true, node:true, mocha: true, esnext:true*/
'use strict';
/**
 * Represents a single change to a document with content diff and roll back functionality
 * @module allstar-publisher/models/editable
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires co
 * @requires diff-match-patch
 * @requires skoodge
 * @requires zip/lib/db
 */

var database         = require( 'zim/lib/db' ).connection
  , co               = require('co')
  , slugify          = require('gaz/string').slugify
  , stripHtmlTags   = require('gaz/string').stripHtmlTags
  , User             = require( 'skoodge' ).getUserModel()
  , ChangeSet        = require('./changeset')
  , diff             = require("../lib/diff")
  , r                = database.default.r
  , fields           = database.default.type
  , Editable
  ;


Editable = database.default.createModel('publisher_editable',{
	  created_at      : fields.date().default( ()=>{return new Date(); } )
  , published_at    : fields.date().allowNull( true ).default( null )
  , title           : fields.string()
  , slug            : fields.string().lowercase().alphanum()
  , content         : fields.string().required()
  , enable_comments : fields.boolean().default( false )
  , version         : fields.number().integer().min(0).allowNull(false).default(0)
});


Editable.define('revision',co.wrap(function* (old_content, old_title, comment, editor){
  let difference, change;

  difference = diff.as_text( this.content, old_content );
  change = new ChangeSet({
      content_diff: difference
     ,old_title: old_title
     ,comment: comment
  });
  change.editor = editor;
  yield change.saveAll({editor:true});
  return change;

}));

Editable.define('revert', co.wrap(function*( at_revision, author ){
    let that = this;
    let changeset = ChangeSet.filter(( change )=>{
        change( 'content_type' )
          .eq( that.getModel().getTableName() )
          .and( change( 'object_id' ).eq( that.id ) )
          .orderBy( r.desc('revision') )
    });
    yield changeset.reapply(author);
}));

Editable.define('toText', function(){
  return stripHtmlTags( this.content );
});

Editable.pre('save',function( next ){
	this.slug = this.slug || slugify( this.title );
	next();
});

module.exports = Editable;
