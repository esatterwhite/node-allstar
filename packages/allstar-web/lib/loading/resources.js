/*jshint laxcomma: true, smarttabs: true, node:true, mocha: true*/
'use strict';
/**
 * Loads resource classes for api from all packages
 * @module allstar-web/lib/loading/resources
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires gaz/fs/loader
 */

var Loader = require( 'gaz/fs/loader' )
  ;

  module.exports = new Loader({
  	filepattern:/\.js$/
    ,searchpath:'resources'
    ,recurse: true
  });
  