/*jshint node:true, laxcomma: true, smarttabs: true, esnext: true*/
'use strict';
/**
 * Module that locates startup / boot scripts for the web server pack instanc
 * @module allstar-web/lib/loading/startup
 * @author Eric satterwhite
 * @since 1.0.0
 * @requires path
 * @requires gaz/class
 * @requires gaz/class/options
 * @requires gaz/lib/loadinger/loader
 */

var  Class   = require( 'gaz/class' )
   , Options = require( 'gaz/class/options' )
   , Loader  = require('gaz/fs/loader')
   , path    = require('path')
   , BootLoader
   ;


/**
 * @alias module:allstar-web/lib/loading/startup
 * @constructor
 * @extends module:allstar-web/lib/loading/loader
 */
BootLoader = new Class({
    inherits:Loader
        ,options:{
                searchpath:'startup'
                ,extensionpattern:/\.js$/
                ,filepattern:/\.js$/
        }
        ,toName: function( app, pth ){
          return pth.replace(this.options.extensionpattern ,'').substr(pth.lastIndexOf( path.sep )+1);
        }
});

var loader = new BootLoader();

/**
 * Locates fixture files located in the project
 * @param {...String} Any number of applications to load scripts from
 * @return Object an object where keys are app names and its value is an array of full paths to all of the fixture files that 
were found
 */
exports.find = function find(){
        return loader.find.apply( loader, arguments );
};

/**
 * Returns An object containe all of the fixture objects listed by app name
 * @param {...String} Any number of applications to load scripts from
 * @return Object an object where keys are app names and its value is an array the fixture data to all of the fixture files 
that were found
 */
exports.load = function load(){
        return loader.load.apply( loader, arguments );
}
exports.Loader = Loader
