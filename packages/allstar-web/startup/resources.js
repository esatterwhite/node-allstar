'use strict';
var tastypie = require('tastypie')
var resources =require('../lib/loading/resources')
var v1 = new tastypie.Api( 'api/v1',{serializer: new tastypie.Serializer() } );
module.exports = function( server ){

	resources
		.load()
		.flat()
		.forEach(( resource )=>{
			v1.use( new resource() )
		})
	server.register([v1],console.log);
}