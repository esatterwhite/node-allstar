var Hapi = require('hapi')
  , tastypie = require('tastypie')
  , Api = tastypie.Api
  , conf = require('keef')
  , Loader = require('gaz/fs/loader')
  , startup = require('./lib/loading/startup')
  , logger = require('bitters')
  , server
  , starthapi
  , v1
  , PORT
  ;

PORT = conf.get('PORT')



server = new Hapi.Server({})
const ui = server.connection({
        host:'0.0.0.0',
        port:PORT,
        labels:['web']
});

const api = server.connection({
        host:'0.0.0.0',
        port:~~PORT + 1,
        labels:['api']
});


// load debug UI & start up plugins
[
    require('inert')
  , require('vision')
].concat( startup.load().flat() )
    .forEach(function( plugin ){
        var attributes // plugin registration tags
          , name       // plugin name / package name
          , version    // plugin semver
          ;


        if( plugin && plugin.register ){
            attributes = plugin.register.attributes;
            name       = attributes.name || attributes.pkg.name;
            version    = attributes.version || attributes.pkg.version;

            logger.debug('loading %s plugin ', name );
            ui.register({
                register:plugin
              , options:( conf.get(name.replace('-','_')) || {} )
            },function( err ){
                if( err ){
                    logger.error( logger.exception.getAllInfo( err ) );
                } else {
                    logger.debug('plugin: %s @ %s loaded', name, version );
                }
            });
        }

        if( typeof plugin === 'function' ){
            logger.debug('executing startup script %s', plugin.name );
            plugin( server );
        }
    });

  if( require.main === module ){
  	server.start( function(){
        logger.info('ui server running at: %s', ui.info.uri);
  			logger.info('api server running at: %s', api.info.uri);
  	})
  }



module.exports = server;
