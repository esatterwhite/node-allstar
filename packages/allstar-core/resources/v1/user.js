/*jshint laxcomma: true, smarttabs: true, node:true, mocha: true, esnext: true*/
'use strict';
/**
 * API for the submission queue
 * @module allstar-publisher/resource/v1/queue
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires tastypie
 * @requires moduleB
 * @requires moduleC
 */

var tastypie = require('tastypie')
  , Resource = require( 'tastypie-rethink' )
  , User = require('skoodge').getUserModel()
  , conf = require('keef')
  , logger = require('bitters')
  , token = require('skoodge/lib/token')
  , hasher = require('skoodge')
  , Mail = require('zim/lib/mail')
  , Boom = require('boom')
  , token = require('skoodge/lib/token')
  , debug = require("debug")('allstar:web:resources:user')
  , exceptions = require('tastypie/lib/exceptions')
  , UserResource
  ;

/**
 * Description
 * @constructor
 * @alias module:queue.js
 * @param {TYPE} [param]
 * @param {TYPE} [?param.val=1j]
 * @example var x = new require('queue.js');
 */
UserResource = Resource.extend({
	options:{
		name:'user'
		,pk:'auth_user_id'
		,queryset: User.filter({}).secure()
		,allowed:{
			list:{ get:true, post: true }
		  , detail:{ get: true }
		  , password_reset_request:{ get: false, post:true }
		  , password_reset_confirm:{ get: true, post:false }
		}
	}

	,fields:{
		username: { type:'date', attribute:'created_at' }
	  , name:{ type:'field'}
	  , roles:{ type:'array' }
	  , permissions:{ type: 'field', readonly: true}
	  , active: { type:'bool' }
	  , superuser: {type:'bool', readonly: true}
	  , loginDate: {type:'date', nullable: true }
	}

	,constructor: function( options ){
		this.parent('constructor', options )
	}

	, dispatch_password: function( action, request, reply ){
		debug('dispatching password reset', action )
		return this.dispatch('password_reset_' + action, this.bundle( request, reply ))
	}

	, prepend_urls: function(){
		return [{
			name:'password_reset_request'
			,path: `${this.options.apiname}/${this.options.name}/{pk}/password/reset`
			, handler: this.dispatch_password.bind( this, 'request' )
			, config: { tags: ['api']}
		},{
			name:'password_reset_confirm'
			,path: `${this.options.apiname}/${this.options.name}/{pk}/password/reset/{code}`
			, handler: this.dispatch_password.bind( this, 'confirm' )
			, config: { tags: ['api']}
		}]
	}

	, get_object: function( bundle, callback ){
		this.options
			.queryset
			._model
			.get( bundle.req.params.pk )
			.withPermissions()
			.secure()
			.run(callback);

		return this;
	}

	/**
	 * DESCRIPTION
	 * @method user.js#<METHODNAME>
	 * @param {TYPE} NAME DESCRIPTION
	 * @param {TYPE} NAME DESCRIPTION
	 * @return {TYPE} DESCRIPTION
	 **/
	,get_password_reset_confirm: function( bundle ){
		return this.respond( bundle, tastypie.http.noContent );
	}

	/**
	 * DESCRIPTION
	 * @method user.js#<METHODNAME>
	 * @param {TYPE} NAME DESCRIPTION
	 * @param {TYPE} NAME DESCRIPTION
	 * @return {TYPE} DESCRIPTION
	 **/
	,post_password_reset_request: function( bundle ){
		var response = tastypie.http.accepted
		  , format = this.format( bundle, this.options.serializer.types )
		  ;

		this.get_object( bundle, (err, user )=>{

			if( !user ){
				return this.user_not_found( bundle );
			}

			if( err ){
				logger.error( logger.exception.getAllInfo( err ) );
				logger.error( err.message );
				err = Boom.internal();
				err.req = bundle.req;
				err.res = bundle.res;
				return this.exception( err );
			}

			this.deserialize( bundle.req.payload, format, ( err, data )=>{
				bundle = this.bundle( bundle.req, bundle.res, data );
				bundle.obj = data;
				let hash = token.encode( user, new Date() )
				this.respond( bundle, response )
			
				Mail.send({
					to:user.email,
					from:conf.get('allstar:mail:default'),
					subject:'reset password',
					template:'password_reset.html',
					data:{
						user: user,
						link: `${this.options.apiname}/${this.options.name}/${user.id}/password/reset/${hash}`
					}
				});
			});
		});
	}

	/**
	 * DESCRIPTION
	 * @method user.js#<METHODNAME>
	 * @param {TYPE} NAME DESCRIPTION
	 * @param {TYPE} NAME DESCRIPTION
	 * @return {TYPE} DESCRIPTION
	 **/
	,user_not_found: function( bundle ){

		bundle.data = new exceptions.BaseException({
			name:'NotFound',
			message:`User with id ${bundle.req.params.pk} not found`,
			code:404,
			type:'user_not_found'
		});
		
		return this.respond( bundle, tastypie.http.notFound )
	}
});

module.exports = UserResource;

